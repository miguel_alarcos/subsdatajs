```javascript
//main.js
import { connect } from 'subsdata'

connect('ws://localhost:8888', store)

//Hello.vue
<template>
  <div class="hello" v-if="$subsReady">
    <h1>el umbral está en {{ max }}:</h1>
    <v-btn @click="create">Crear contador a 0</v-btn>
    <transition-group name="fade" tag="div">
      <div class="pointer" @click="inc(c.id, 1)" v-bind:key="c.id" v-for="c in myCounters">
        {{ c.x }}
      </div>  
    </transition-group>
    <v-btn @click="suma">2 + 3 = </v-btn>
    <span>{{valor}}</span> 
    <v-btn @click="$closeSocket">cerrar socket</v-btn>
  </div>
  <div v-else>
    Loading...
  </div>
</template>

<script>
import { SDP_Mixin, sdpComputed } from 'subsdata'

export default {
  name: 'HelloWorld',
  mixins: [SDP_Mixin],
  props: {
    msg: String
  },
  predicates: {
    maxChange: ['x_less_than', 'max']
  },
  data(){
    return {
      valor: null,
      max: 5
    }
  },
  computed: {
    maxChange(){
      return sdpComputed(this, 'max')
    },
    myCounters(){
      return this.$store.state.sdp.subs.x_less_than
    }
  }
  methods: {
    async create(){
      await this.$rpc('create', {})
    },
    async suma(){
      this.valor = await this.$rpc('add', {a: 2, b: 3})
    },
    async inc(id, value){
      this.$rpc('increment', {id, value})
    }
  }
}
</script>
